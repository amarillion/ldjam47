const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
	mode: process.env.NODE_ENV === 'development' ? 'development' : 'production',
	output: {
		filename: '[name].[contenthash].js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader"
				}
			},
			{
				test: [/\.vert$/, /\.frag$/],
				use: "raw-loader"
			},
			{
				test: /\.(gif|png|jpe?g|svg|xml|ogg)$/i,
				use: "file-loader"
			}
		]
	},
	optimization: {
		splitChunks: {
			chunks: 'all'
		},
		runtimeChunk: 'single'
	},
	plugins: [
		new CleanWebpackPlugin({
			root: path.resolve(__dirname, "../")
		}),
		new webpack.DefinePlugin({
			CANVAS_RENDERER: JSON.stringify(true),
			WEBGL_RENDERER: JSON.stringify(true)
		}),
		new HtmlWebpackPlugin({
			template: "./index.html"
		})
	],
	node: false,
};
