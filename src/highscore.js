const SERVER_URL = "https://stats.simon-lenz.de";

export async function sendStatistics({ username, score, orbits }) {
    const content = {
        username,
		score,
		orbits
    }

	try {
		const response = await fetch(`${SERVER_URL}/submit-statistics`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(content)
		})
		return await response.json();
	}
	catch {
		return [];
	}
}

export async function getStatistics() {
	try {
		const resp = await fetch(`${SERVER_URL}/statistics`);
		const result = await resp.json();
		const entryList = result.entries.map(entry => entry.submittedData);
		entryList.sort((o1, o2) => o2.score - o1.score);
		return entryList.slice(0, 10);
	}
	catch {
		return [];
	}
}
