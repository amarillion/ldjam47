import { sendStatistics } from '../highscore';


export class GameOverScene extends Phaser.Scene {

	constructor () {
		super({ key: 'GameOverScene' });
	}

	create({ score = 0, currentLevel = 0 }) {
		console.log("Game Over")
		this.add.text(150, 150, 'GAME OVER');

		this.time.addEvent({
			delay: 1000,
			callback: async () => {
				let username = prompt('Under which name would you like to go down in history?');
				if (username !== null) {
					await sendStatistics({ username, score: score, orbits: currentLevel });
				}
				this.scene.start('MenuScene');
			},
			callbackScope: this,
			loop: false
		});
	}
}
