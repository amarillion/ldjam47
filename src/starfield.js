class OrbitalParticle extends Phaser.GameObjects.Particles.Particle {

	constructor(emitter) {
		super(emitter);

		this.t = Math.PI / 4;
		this.r = Math.random();
	}

	update(delta, step, processors) {
		// super.update(delta, step, processors);

		const { x, y } = this.emitter.orbitalCoords.convertToScreen(this.t, this.r);
		this.x = x;
		this.y = y;

		this.t += this.emitter.angularSpeed;

		this.lifeCurrent -= delta;
		return (this.lifeCurrent <= 0);
	}
}

const DEFAULT_PARTICLES_SPEEDS = [ -160/-3e4, -120/-3e4, -80/-3e4 ];

export default class StarField {

	constructor(scene, orbitalCoords) {
		const particles = scene.add.particles('flares');
		this.e1 = particles.createEmitter({
			frames: 0,
			frequency: 100,
			x: -100, y: -100,
			maxParticles: 200,
			lifespan: NaN,
			scale: 1.0,
			blendMode: 'ADD',
			particleClass: OrbitalParticle
		});
		this.e1.orbitalCoords = orbitalCoords;

		this.e2 = particles.createEmitter({
			frames: 1,
			frequency: 100,
			x: -100, y: -100,
			speedX: 0, speedY: 0,
			maxParticles: 250,
			lifespan: NaN,
			scale: 0.8,
			blendMode: 'ADD',
			particleClass: OrbitalParticle
		});
		this.e2.orbitalCoords = orbitalCoords;

		this.e3 = particles.createEmitter({
			frames: 2,
			frequency: 100,
			x: -100, y: -100,
			lifespan: NaN,
			maxParticles: 300,
			scale: 0.5,
			blendMode: 'ADD',
			particleClass: OrbitalParticle
		});
		this.e3.orbitalCoords = orbitalCoords;

		this.speedFactor = 1.0;
	}

	set speedFactor(value) {
		this.e1.angularSpeed = DEFAULT_PARTICLES_SPEEDS[0] * value;
		this.e2.angularSpeed = DEFAULT_PARTICLES_SPEEDS[1] * value;
		this.e3.angularSpeed = DEFAULT_PARTICLES_SPEEDS[2] * value;
	}

}
