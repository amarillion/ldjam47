import Phaser from 'phaser';

export class Fuel extends Phaser.GameObjects.GameObject {

	constructor(scene, start, velocity) {
		super(scene);
		this.start = start;

		this.scene.addItem(this);

		this.orbitalCoords = scene.orbitalCoords;

		const { t, r } = this.orbitalCoords.fromScreenCoords(start.x, start.y);
		this.t = t;
		this.r = r;

		this.sprite = this.scene.physics.add.sprite(this.start.x, this.start.y, 'fuel');
		this.sprite.play('fuel-normal', true);
		Object.assign(this.sprite.body.offset, { x: 8, y: 8 });
		Object.assign(this.sprite.body, { height: 16, width: 16 });
	}

	preUpdate(time, delta) {
		const { x, y } = this.orbitalCoords.convertToScreen(this.t, this.r);
		this.sprite.setPosition(x, y);

		this.t += this.angularSpeed;

		if (x < -40) {
			this.removeAndDestroy();
		}
	}

	removeAndDestroy() {
		this.scene.removeItem(this);
		this.destroy();
	}

	destroy() {
		this.sprite.destroy();
		super.destroy();
	}
}
