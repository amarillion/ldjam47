import Phaser from 'phaser';

export class Alien extends Phaser.GameObjects.GameObject {

	constructor(scene, start, movementType) {
		super(scene);
		this.start = start;
		this.movementType = movementType;

		this.scene.addEnemy(this);

		this.sprite = this.scene.physics.add.sprite(this.start.x, this.start.y, 'enemy-alien');
		this.sprite.play('alien-normal', true);
		Object.assign(this.sprite.body.offset, { x: 8, y: 8 });
		Object.assign(this.sprite.body, { height: 16, width: 16 });
	}

	preUpdate(time, delta) {
		if (this.exploded) {
			return;
		}
		if (this.movementType === 'up-wait-down-wait') {
			const step = Math.floor(time / 1000) % 4; // number between 0 and 3 which increases by one every second
			const verticalVelocity = 120;
			if (step === 0) {
				this.sprite.body.setVelocityY(verticalVelocity);
			}
			else if (step === 1 || step === 3) {
				this.sprite.body.setVelocityY(0);
			}
			else if (step === 2) {
				this.sprite.body.setVelocityY(-verticalVelocity);
			}

		}
		else if (this.movementType === 'circles') {
			this.sprite.body.setVelocityX(-100-150*Math.cos(1.5*time/180));
			this.sprite.body.setVelocityY(400*Math.sin(1.5*time/180));
		}

		if (this.x < -40) {
			this.removeAndDestroy();
		}
	}

	explode() {
		if (this.exploded) {
			return;
		}
		this.exploded = true;
		this.scene.playEffect('explCrackle');
		const oldVelocity = this.sprite.body.velocity;
		const SLOWDOWN_ON_DEATH = 0.1
		this.sprite.body.setVelocity(oldVelocity.x * SLOWDOWN_ON_DEATH, oldVelocity.y * SLOWDOWN_ON_DEATH);
		this.sprite.body.setAcceleration(0, 0);
		this.sprite.play('alien-explosion', false);
		this.sprite.once(Phaser.Animations.Events.SPRITE_ANIMATION_COMPLETE, () => {
			this.removeAndDestroy();
		});
	}

	removeAndDestroy() {
		this.scene.removeEnemy(this);
		this.destroy();
	}

	destroy() {
		this.sprite.destroy();
		super.destroy();
	}

}
