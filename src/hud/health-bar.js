import { MAX_LIVES } from "../constants";

export class HealthBar {
	constructor({ scene, x, y }) {
		this.sprites = [];
		for (let i = 0; i < MAX_LIVES; i++) {
			const sprite = scene.add.sprite(x - 24 * i, y, 'heart');
			sprite.setFrame(0);
			this.sprites.push(sprite);
		}
	}

	render({ time, delta, player }) {
		for (let i = 0; i < this.sprites.length; i++) {
			if (i + 1 <= player.lives) {
				this.sprites[i].setFrame(0);
			}
			else {
				this.sprites[i].setFrame(1);
			}
		}
	}
}
