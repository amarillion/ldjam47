import logo from "./assets/logo.png";

export class MenuComponent extends HTMLElement {

	constructor() {
		super();
		this.attachShadow({ mode: 'open' });
		this.render([]);
	}

	set stats(value) {
		console.log(`stats set to ${value}`);
		this.render(value);
		this.connectedCallback();
	}

	generateHighscoreTable(stats) {
		let res = "<table id='stats-table'>\n"
		res += "<thead>\n";
		res += "<tr>\n";
		res += "<th></th>\n";
		res += "<th>Name</th>\n";
		res += "<th>Orbits</th>\n";
		res += "<th>Score</th>\n";
		res += "</tr>\n";
		res += "</thead>\n";
		res += "<tbody>\n";
		for (let i = 0; i < stats.length; i++) {
			const stat = stats[i];
			const username = stat.username ? stat.username.length > 20 ? stat.username.substr(0, 20) + '…' : stat.username : "unknown";
			res += "<tr>\n";
			res += `<td>#${i + 1}</td>\n`;
			res += `<td>${username}</td>\n`;
			res += `<td>${stat.orbits}</td>\n`;
			res += `<td>${stat.score}</td>\n`;
			res += "</tr>\n";
		}
		res += "</tbody>\n";
		return res;
	}

	render(stats) {
		const highscoreTable = stats.length > 0 ? this.generateHighscoreTable(stats) : "No Highscores yet.";

		this.shadowRoot.innerHTML = `
		<style>
			:host {
				display: flex;
				justify-content: center;
				align-items: center;
				height: 100vh;

				background-image: url(${logo});
				background-position: center;
				background-repeat: no-repeat;
				background-size: contain;
			}

			button {
				background-color: #4C50AF; /* Blue */
				border: none;
				color: white;
				text-align: center;
				text-decoration: none;
				display: inline-block;
				font-size: 16px;
				margin: 2rem;
				width: 10rem;
				height: 4rem;
			}
			button:hover {
				background-color: #6C70DF; /* Blue */
			}
			#stats-table {
				position: fixed;
				background-color: #dfc88555;
				color: #323125;
				font-family: monospace;
				right: 60%;
				bottom: 20px;
				border-collapse: separate;
				border-spacing: 1em 1em;
			}
			th {
				max-width: 50px;
			}
		</style>

		<div class="main">
			<div class="buttonBar">
				<button id="startGame">Start Game</button>
			</div>
			${highscoreTable}
		</div>
	`;
	}

	connectedCallback() {
		this.shadowRoot.querySelector("#startGame").addEventListener("click", (event) => {
			this.dispatchEvent(new CustomEvent("Start"));
			this.dispatchEvent(new CustomEvent("button-pressed"));
		});
	}

}
